# Cafeteria-front - Requisitos
- Version de Angular ^14.2.0
- Node ^16.17.0

# Paso 1 - Instalar dependencias
Ejercutar comando `npm install`

# Paso 2 - Correr proyecto
Ejercutar comando `ng s -o`

# Recomendaciones
Se recomienda primero tener el backend funcionando
