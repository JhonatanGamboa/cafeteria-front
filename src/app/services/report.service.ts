import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JsonResponseInterfaces } from '../interfaces/json-response.interface';
import { ProductSaleInterface } from '../interfaces/product-sale.interface';
import { ProductStockInterface } from '../interfaces/product-stock.interface';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpClient) {}

  getReportSale(): Observable<JsonResponseInterfaces<ProductSaleInterface>> {
    return this.http.get<JsonResponseInterfaces<ProductSaleInterface>>(
      `${environment.URL_API}product-sale`
    );
  }

  getReportStock(): Observable<JsonResponseInterfaces<ProductStockInterface>>{
    return this.http.get<JsonResponseInterfaces<ProductStockInterface>>(
      `${environment.URL_API}product-stock`
    );
  }
}
