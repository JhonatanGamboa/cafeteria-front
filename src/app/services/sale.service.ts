import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SaleInterface } from '../interfaces/sale.interface';
import { Observable } from 'rxjs';
import { JsonResponseInterfaces } from '../interfaces/json-response.interface';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class SaleService {
  constructor(private http: HttpClient) {}

  private url: string = 'sale/';

  sendSale(
    formData: SaleInterface
  ): Observable<JsonResponseInterfaces<SaleInterface>> {
    return this.http.post<JsonResponseInterfaces<SaleInterface>>(
      `${environment.URL_API}${this.url}`,
      formData
    );
  }
}
