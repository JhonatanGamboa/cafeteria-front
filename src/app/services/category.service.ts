import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CategoryInterface } from '../interfaces/category.interface';
import { JsonResponseInterfaces } from '../interfaces/json-response.interface';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  private url: string = 'category/';

  getCategories(): Observable<JsonResponseInterfaces<CategoryInterface[]>> {
    return this.http.get<JsonResponseInterfaces<CategoryInterface[]>>(
      `${environment.URL_API}${this.url}`
    );
  }
}
