import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JsonResponseInterfaces } from '../interfaces/json-response.interface';
import { ProductInterface } from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  private url: string = 'product/';

  getProducts(): Observable<JsonResponseInterfaces<ProductInterface[]>> {
    return this.http.get<JsonResponseInterfaces<ProductInterface[]>>(
      `${environment.URL_API}${this.url}`
    );
  }

  getProduct(productId: string): Observable<JsonResponseInterfaces<ProductInterface>>{
    return this.http.get<JsonResponseInterfaces<ProductInterface>>(
      `${environment.URL_API}${this.url}${productId}`
    );
  }

  postProduct(
    formData: ProductInterface
  ): Observable<JsonResponseInterfaces<ProductInterface>> {
    return this.http.post<JsonResponseInterfaces<ProductInterface>>(
      `${environment.URL_API}${this.url}`,
      formData
    );
  }

  putProduct(
    formData: ProductInterface,
    productId: string
  ): Observable<JsonResponseInterfaces<ProductInterface>> {
    return this.http.put<JsonResponseInterfaces<ProductInterface>>(
      `${environment.URL_API}${this.url}${productId}`,
      formData
    );
  }

  deleteProduct(
    productId: number
  ): Observable<JsonResponseInterfaces<ProductInterface>> {
    return this.http.delete<JsonResponseInterfaces<ProductInterface>>(
      `${environment.URL_API}${this.url}${productId}`
    );
  }
}
