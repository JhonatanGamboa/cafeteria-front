export interface ProductSaleInterface {
    product_id:   number;
    product_name: string;
    total_amount: string;
    total_sales:  string;
}
