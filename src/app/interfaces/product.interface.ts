import { CategoryInterface } from "./category.interface";

export interface ProductInterface {
  id:        number;
  name:      string;
  reference: string;
  price:     number;
  weight:    number;
  stock:     number;
  category:  CategoryInterface;
  created_at: Date
}
