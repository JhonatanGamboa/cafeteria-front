export interface ProductStockInterface {
  product_id:   number;
  product_name: string;
  stock:        number;
  category:     string;
}
