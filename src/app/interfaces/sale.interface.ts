import { ProductInterface } from "./product.interface";

export interface SaleInterface {
  id?:      number;
  product_id?: number,
  amount?: number,
  total?:   number;
  product?: ProductInterface;
}
