import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewProductComponent } from './views/new-product/new-product.component';
import { ProductComponent } from './views/product/product.component';
import { ReportComponent } from './views/report/report.component';
import { ShopComponent } from './views/shop/shop.component';

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full'},
  { path: 'products', component: ProductComponent },
  { path: 'new-product', component: NewProductComponent },
  { path: 'new-product/:id', component: NewProductComponent },
  { path: 'report', component: ReportComponent },
  { path: 'shop', component: ShopComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
