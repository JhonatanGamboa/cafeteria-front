import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductInterface } from 'src/app/interfaces/product.interface';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {

  products: ProductInterface[] = [];
  isLoading: boolean = false;

  constructor(
    private _productService: ProductService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(){
    this.isLoading = true;
    this._productService.getProducts().subscribe({
      next: (resp) => {
        this.products = resp.data
      },
      error(err) {
        console.log('getProducts', err);
      },
      complete: () =>{
        this.isLoading = false;
      }
    })
  }

  newProduct(){
    this.route.navigateByUrl('new-product');
  }

  updateProduct(productId: number){
    this.route.navigate(['new-product', productId]);

  }
  deleteProduct(productId: number){
    Swal.fire({
      title: '¿Esta seguro que desea eliminar el producto?',
      showDenyButton: true,
      showConfirmButton: false,
      showCancelButton: true,
      denyButtonText: `Eliminar`,
    }).then((result) => {
      if (result.isDenied) {
        this._productService.deleteProduct(productId).subscribe({
          next:()=>{
            this.getProducts();
          },
          error(err) {
            console.log('deleteProduct',err);
          },
          complete:()=>{
            Swal.fire('Producto eliminado')
          }
        })
      }
    })
  }

}
