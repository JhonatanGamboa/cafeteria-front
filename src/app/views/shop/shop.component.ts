import { Component, OnInit } from '@angular/core';
import { ProductInterface } from 'src/app/interfaces/product.interface';
import { SaleInterface } from 'src/app/interfaces/sale.interface';
import { ProductService } from 'src/app/services/product.service';
import { SaleService } from 'src/app/services/sale.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.less'],
})
export class ShopComponent implements OnInit {
  products: ProductInterface[] = [];
  isLoading: boolean = false;

  constructor(
    private _productService: ProductService,
    private _saleService: SaleService
  ) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.isLoading = true;
    this._productService.getProducts().subscribe({
      next: (resp) => {
        this.products = resp.data;
      },
      error(err) {
        console.log('getProducts', err);
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }

  async sendSale(product: ProductInterface) {
    Swal.fire({
      title: 'Ingrese la cantidad a comprar',
      input: 'number',
      showCancelButton: true,
      confirmButtonText: 'Comprar!',
    }).then((result) => {

      if (product.stock < result.value) {
        this.isLoading = false;
        Swal.fire('La cantidad solicitda supera al stock!!');
        return;
      }

      if (result.isConfirmed) {
        this.isLoading = true;
        const data: SaleInterface = {
          product_id: product.id,
          amount: result.value,
        };

        this._saleService.sendSale(data).subscribe({
          next: () => {
            Swal.fire('Se ha comprado el producto!');
          },
          error: (err) => {
            Swal.fire(err.error.message);
          },
          complete: () => {
            this.getProducts();
          },
        });
      }
    });
  }
}
