import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop/shop.component';
import { ProductComponent } from './product/product.component';
import { ReportComponent } from './report/report.component';
import { NewProductComponent } from './new-product/new-product.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ShopComponent,
    ProductComponent,
    ReportComponent,
    NewProductComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [
    ShopComponent,
    ProductComponent,
    ReportComponent,
    NewProductComponent,
  ],
})
export class ViewsModule {}
