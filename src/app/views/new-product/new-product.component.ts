import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryInterface } from 'src/app/interfaces/category.interface';
import { ProductInterface } from 'src/app/interfaces/product.interface';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.less'],
})
export class NewProductComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  categories: CategoryInterface[] = [];
  isLoading = true;
  productId: string;
  title: string = '';
  button: string = '';

  constructor(
    private fb: FormBuilder,
    private _categoryService: CategoryService,
    private _productService: ProductService,
    private route: Router,
    private activateRoute: ActivatedRoute
  ) {
    this.productId = this.activateRoute.snapshot.paramMap.get('id') ?? '';
  }

  ngOnInit(): void {
    this.productId ? this.getProduct() : this.makeForm();
    if (this.productId) {
      this.title = 'Actualizar producto';
      this.button = 'Actualizar';
    } else {
      this.makeForm();
      this.title = 'Crear producto';
      this.button = 'Crear';
    }
    this.getCategories();
  }

  makeForm(productData?: ProductInterface) {
    this.form = this.fb.group({
      name: [
        productData?.name ?? '',
        [Validators.required, Validators.minLength(2)],
      ],
      price: [productData?.price ?? '', [Validators.required]],
      stock: [productData?.stock ?? '', [Validators.required]],
      weight: [productData?.weight ?? '', [Validators.required]],
      category_id: [productData?.category.id ?? '', [Validators.required]],
    });
    this.isLoading = false;
  }

  getCategories() {
    this._categoryService.getCategories().subscribe({
      next: (resp) => {
        this.categories = resp.data;
      },
      error(err) {
        console.log('getCategories', err);
      },
    });
  }

  getProduct() {
    console.log('entra');

    this._productService.getProduct(this.productId).subscribe({
      next: (resp) => {
        this.makeForm(resp.data);
      },
      error(err) {
        console.log(err);
      },
    });
  }

  sendProduct() {
    this.isLoading = true;

    if (!this.productId) {
      this._productService.postProduct(this.form.value).subscribe({
        next: () => {
          Swal.fire('Se ha añadido un nuevo producto!');
        },
        error(err) {
          Swal.fire(err.error.message);
          console.log('postProduct', err);
        },
        complete: () => {
          this.isLoading = false;
          this.route.navigateByUrl('products');
        },
      });
    } else {
      this._productService
        .putProduct(this.form.value, this.productId)
        .subscribe({
          next: () => {
            Swal.fire('Se ha actualizado el producto!');
          },
          error(err) {
            Swal.fire(err.error.message);
            console.log('sendProduct', err);
          },
          complete: () => {
            this.isLoading = false;
            this.route.navigateByUrl('products');
          },
        });
    }
  }
}
