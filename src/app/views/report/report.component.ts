import { Component, OnInit } from '@angular/core';
import { ProductSaleInterface } from 'src/app/interfaces/product-sale.interface';
import { ProductStockInterface } from 'src/app/interfaces/product-stock.interface';
import { ReportService } from 'src/app/services/report.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.less'],
})
export class ReportComponent implements OnInit {
  reportSale!: ProductSaleInterface;
  reportStock!: ProductStockInterface;

  constructor(private _reportService: ReportService) {}

  ngOnInit(): void {

    combineLatest([
      this._reportService.getReportSale(),
      this._reportService.getReportStock()
    ]).subscribe({
      next:([resp1, resp2])=>{
        this.reportSale = resp1.data;
        this.reportStock = resp2.data;
      },
      error(err) {
        console.log(err);
      },
    })
  }
}
